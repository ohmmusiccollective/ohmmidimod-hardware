![OHMMIDIMOD Logo](./OHMMIDIMOD.svg "OHMMIDIMOD Logo")

# OHMMIDIMOD

OHMMIDIMOD is an open-source USB adapter that provides USB-MIDI capability to Roland JUNO-60 synthesizers without the need for hardware modification.
The OHMMIDIMOD plugs into the DCB port on the JUNO's rear panel.
As of right now, it only supports sending MIDI data from the host to the JUNO.
It is not possible to use the JUNO as a MIDI keyboard.

This repository only contains the KiCad project files and the Gerber plot files. The firmware is found in a separate repository: <https://gitlab.com/ohmmusiccollective/ohmmidimod-firmware>

## Assembly

### Caveats
Double-check component polarity before soldering. The fabrication layers in KiCad indicate the correct polarity. For components U1 and U2, ensure the correct orientation by checking the position of the "pin-1" marker. LEDs D1 and D2 have a green arrow on the back side of the package. Ensure that it matches the direcition of the arrow on the fabrication layer.
The polarity of tantalum capacitor C3 is indicated by a bar marker on the package, which is also present on the fabrication layer. Any other components used in the design are eiter not polar, or they are impossible to place incorrectly, such as the DCB and USB connector for example.

## Resources

- [JUNO-60 Service Notes](<https://manuals.fdiskc.com/flat/Roland%20Juno-60%20Service%20Notes%20(%20HI-RES%20).pdf>)
- [midi-to-dcb](https://code.google.com/archive/p/midi-to-dcb/)

## Acknowledgements

Special thanks go out to [Bastli](https://bastli.ethz.ch/) at ETH Zurich who let us use their soldering equipment.
