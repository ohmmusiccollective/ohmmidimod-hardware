EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "OHMMIDIMOD"
Date "2021-05-06"
Rev "B"
Comp "OHMMUSICCOLLECTIVE"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x07_Top_Bottom J3
U 1 1 601937DE
P 10500 5000
F 0 "J3" H 10450 5550 50  0000 L CNN
F 1 "DCB" H 10450 5450 50  0000 L CNN
F 2 "REV_B:112-014-113R001" H 10500 5000 50  0001 C CNN
F 3 "https://www.norcomp.net/rohspdfs/SCSI-050Ribbon/11Y/112/112-YYY-113R001.pdf" H 10500 5000 50  0001 C CNN
	1    10500 5000
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATmega:ATmega16U2-AU U1
U 1 1 60196724
P 4900 5300
F 0 "U1" H 5050 3900 50  0000 L CNN
F 1 "ATmega16U2-AU" H 5050 3800 50  0000 L CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 4900 5300 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc7799.pdf" H 4900 5300 50  0001 C CNN
	1    4900 5300
	1    0    0    -1  
$EndComp
Text Label 10100 4700 2    50   ~ 0
JUNO_RX_BUSY_SI
Wire Wire Line
	10150 5000 10300 5000
Text Label 10100 4800 2    50   ~ 0
JUNO_RX_DATA_DO
Text Label 10100 4900 2    50   ~ 0
JUNO_RX_CLOCK_CO
$Comp
L power:GND #PWR012
U 1 1 60199230
P 10150 5500
F 0 "#PWR012" H 10150 5250 50  0001 C CNN
F 1 "GND" H 10155 5327 50  0000 C CNN
F 2 "" H 10150 5500 50  0001 C CNN
F 3 "" H 10150 5500 50  0001 C CNN
	1    10150 5500
	1    0    0    -1  
$EndComp
$Comp
L REV_B-rescue:USB_B-Connector J1
U 1 1 60199726
P 1150 6600
F 0 "J1" H 950 7100 50  0000 L CNN
F 1 "USB_B" H 950 7000 50  0000 L CNN
F 2 "Connector_USB:USB_B_OST_USB-B1HSxx_Horizontal" H 1300 6550 50  0001 C CNN
F 3 "http://www.on-shore.com/wp-content/uploads/USB-B1HSXX.pdf" H 1300 6550 50  0001 C CNN
	1    1150 6600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 6019C68D
P 3950 5600
F 0 "#PWR07" H 3950 5350 50  0001 C CNN
F 1 "GND" H 3955 5427 50  0000 C CNN
F 2 "" H 3950 5600 50  0001 C CNN
F 3 "" H 3950 5600 50  0001 C CNN
	1    3950 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 5600 3950 5550
Wire Wire Line
	3950 5250 3950 5100
$Comp
L Device:R R2
U 1 1 6019D000
P 2000 6600
F 0 "R2" V 1793 6600 50  0000 C CNN
F 1 "22" V 1884 6600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 1930 6600 50  0001 C CNN
F 3 "~" H 2000 6600 50  0001 C CNN
	1    2000 6600
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 6019D69F
P 2000 6700
F 0 "R3" V 2100 6700 50  0000 C CNN
F 1 "22" V 2200 6700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 1930 6700 50  0001 C CNN
F 3 "~" H 2000 6700 50  0001 C CNN
	1    2000 6700
	0    1    1    0   
$EndComp
Wire Wire Line
	1850 6600 1450 6600
Wire Wire Line
	1450 6700 1850 6700
$Comp
L power:GND #PWR09
U 1 1 6019E4EE
P 4800 6900
F 0 "#PWR09" H 4800 6650 50  0001 C CNN
F 1 "GND" H 4805 6727 50  0000 C CNN
F 2 "" H 4800 6900 50  0001 C CNN
F 3 "" H 4800 6900 50  0001 C CNN
	1    4800 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 6700 4800 6800
Wire Wire Line
	4900 6700 4900 6800
Wire Wire Line
	4900 6800 4800 6800
Connection ~ 4800 6800
Wire Wire Line
	4800 6800 4800 6900
$Comp
L power:GND #PWR03
U 1 1 6019ED8A
P 1150 7200
F 0 "#PWR03" H 1150 6950 50  0001 C CNN
F 1 "GND" H 1155 7027 50  0000 C CNN
F 2 "" H 1150 7200 50  0001 C CNN
F 3 "" H 1150 7200 50  0001 C CNN
	1    1150 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 7000 1150 7100
$Comp
L power:VBUS #PWR05
U 1 1 6019F598
P 1600 6200
F 0 "#PWR05" H 1600 6050 50  0001 C CNN
F 1 "VBUS" H 1615 6373 50  0000 C CNN
F 2 "" H 1600 6200 50  0001 C CNN
F 3 "" H 1600 6200 50  0001 C CNN
	1    1600 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 6200 1600 6400
Wire Wire Line
	1600 6400 1450 6400
$Comp
L power:VBUS #PWR08
U 1 1 601A0311
P 4800 3250
F 0 "#PWR08" H 4800 3100 50  0001 C CNN
F 1 "VBUS" H 4815 3423 50  0000 C CNN
F 2 "" H 4800 3250 50  0001 C CNN
F 3 "" H 4800 3250 50  0001 C CNN
	1    4800 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 601A15CC
P 1000 2650
F 0 "R1" H 1070 2696 50  0000 L CNN
F 1 "100k" H 1070 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 930 2650 50  0001 C CNN
F 3 "~" H 1000 2650 50  0001 C CNN
	1    1000 2650
	1    0    0    -1  
$EndComp
$Comp
L power:VBUS #PWR01
U 1 1 601A406E
P 1000 2300
F 0 "#PWR01" H 1000 2150 50  0001 C CNN
F 1 "VBUS" H 1015 2473 50  0000 C CNN
F 2 "" H 1000 2300 50  0001 C CNN
F 3 "" H 1000 2300 50  0001 C CNN
	1    1000 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 601A4646
P 1000 3300
F 0 "#PWR02" H 1000 3050 50  0001 C CNN
F 1 "GND" H 1005 3127 50  0000 C CNN
F 2 "" H 1000 3300 50  0001 C CNN
F 3 "" H 1000 3300 50  0001 C CNN
	1    1000 3300
	1    0    0    -1  
$EndComp
Text Label 4150 4200 2    50   ~ 0
RESET
NoConn ~ 10800 4700
NoConn ~ 10800 4800
NoConn ~ 10800 4900
NoConn ~ 10800 5000
NoConn ~ 10800 5100
$Comp
L Device:Crystal Y1
U 1 1 601ABC55
P 1400 4600
F 0 "Y1" H 1400 5000 50  0000 C CNN
F 1 "16MHz" H 1400 4900 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_ECS_CSM3X-2Pin_7.6x4.1mm" H 1400 4600 50  0001 C CNN
F 3 "https://ecsxtal.com/store/pdf/CSM-3X.pdf" H 1400 4600 50  0001 C CNN
	1    1400 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 601AC7D0
P 1700 4950
F 0 "C2" H 1815 4996 50  0000 L CNN
F 1 "20p" H 1815 4905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1738 4800 50  0001 C CNN
F 3 "~" H 1700 4950 50  0001 C CNN
	1    1700 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 601AD29D
P 1100 4950
F 0 "C1" H 1215 4996 50  0000 L CNN
F 1 "20p" H 1215 4905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 1138 4800 50  0001 C CNN
F 3 "~" H 1100 4950 50  0001 C CNN
	1    1100 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 4600 1700 4600
$Comp
L power:GND #PWR04
U 1 1 601AFCB4
P 1100 5200
F 0 "#PWR04" H 1100 4950 50  0001 C CNN
F 1 "GND" H 1105 5027 50  0000 C CNN
F 2 "" H 1100 5200 50  0001 C CNN
F 3 "" H 1100 5200 50  0001 C CNN
	1    1100 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 601AFF5C
P 1700 5200
F 0 "#PWR06" H 1700 4950 50  0001 C CNN
F 1 "GND" H 1705 5027 50  0000 C CNN
F 2 "" H 1700 5200 50  0001 C CNN
F 3 "" H 1700 5200 50  0001 C CNN
	1    1700 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 5200 1700 5100
Wire Wire Line
	1100 5100 1100 5200
NoConn ~ 5600 4200
NoConn ~ 5600 4600
NoConn ~ 5600 4700
NoConn ~ 5600 4800
NoConn ~ 5600 5200
NoConn ~ 5600 5300
NoConn ~ 5600 5400
NoConn ~ 5600 5500
NoConn ~ 5600 5800
NoConn ~ 5600 5900
NoConn ~ 5600 6300
NoConn ~ 5600 6400
$Comp
L Device:C C4
U 1 1 601C15A4
P 3650 3500
F 0 "C4" H 3765 3546 50  0000 L CNN
F 1 "0.1u" H 3765 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3688 3350 50  0001 C CNN
F 3 "~" H 3650 3500 50  0001 C CNN
	1    3650 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 601C21C0
P 4050 3500
F 0 "C5" H 4165 3546 50  0000 L CNN
F 1 "0.1u" H 4165 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4088 3350 50  0001 C CNN
F 3 "~" H 4050 3500 50  0001 C CNN
	1    4050 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 601C26CC
P 4450 3500
F 0 "C6" H 4565 3546 50  0000 L CNN
F 1 "0.1u" H 4565 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4488 3350 50  0001 C CNN
F 3 "~" H 4450 3500 50  0001 C CNN
	1    4450 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 601C29E5
P 4050 3750
F 0 "#PWR011" H 4050 3500 50  0001 C CNN
F 1 "GND" H 4055 3577 50  0000 C CNN
F 2 "" H 4050 3750 50  0001 C CNN
F 3 "" H 4050 3750 50  0001 C CNN
	1    4050 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 3650 4050 3700
Wire Wire Line
	4450 3650 4450 3700
Wire Wire Line
	4450 3700 4050 3700
Connection ~ 4050 3700
Wire Wire Line
	4050 3700 4050 3750
Wire Wire Line
	3650 3700 3650 3650
Wire Wire Line
	3650 3700 4050 3700
Wire Wire Line
	4450 3350 4450 3300
Wire Wire Line
	4450 3300 4050 3300
Connection ~ 4050 3300
Wire Wire Line
	4050 3300 4050 3350
Wire Wire Line
	3650 3300 3650 3350
Wire Wire Line
	3650 3300 4050 3300
Wire Wire Line
	1050 7000 1050 7100
Wire Wire Line
	1050 7100 1150 7100
Connection ~ 1150 7100
Wire Wire Line
	1150 7100 1150 7200
$Comp
L REV_B:AVR-ISP-6 J2
U 1 1 601E5D7E
P 7900 1800
F 0 "J2" H 8350 2350 50  0000 R CNN
F 1 "AVR-ISP-6" H 8350 2250 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" V 7650 1850 50  0001 C CNN
F 3 " ~" H 6625 1250 50  0001 C CNN
	1    7900 1800
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 601E712C
P 8000 2200
F 0 "#PWR014" H 8000 1950 50  0001 C CNN
F 1 "GND" H 8005 2027 50  0000 C CNN
F 2 "" H 8000 2200 50  0001 C CNN
F 3 "" H 8000 2200 50  0001 C CNN
	1    8000 2200
	1    0    0    -1  
$EndComp
$Comp
L power:VBUS #PWR013
U 1 1 601E89CC
P 8000 1300
F 0 "#PWR013" H 8000 1150 50  0001 C CNN
F 1 "VBUS" H 8015 1473 50  0000 C CNN
F 2 "" H 8000 1300 50  0001 C CNN
F 3 "" H 8000 1300 50  0001 C CNN
	1    8000 1300
	1    0    0    -1  
$EndComp
Text Label 5700 4500 0    50   ~ 0
ISP_PDO
Text Label 5700 4400 0    50   ~ 0
ISP_PDI
$Comp
L Switch:SW_Push SW1
U 1 1 601BA0A8
P 1000 3100
F 0 "SW1" V 954 3248 50  0000 L CNN
F 1 "SW_Push" V 1045 3248 50  0001 L CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_Vertical_Wuerth_434133025816" H 1000 3300 50  0001 C CNN
F 3 "https://www.we-online.com/catalog/datasheet/434133025816.pdf" H 1000 3300 50  0001 C CNN
	1    1000 3100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 601AB427
P 1800 6200
F 0 "TP1" H 1858 6318 50  0000 L CNN
F 1 "TestPoint" H 1858 6227 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 2000 6200 50  0001 C CNN
F 3 "~" H 2000 6200 50  0001 C CNN
	1    1800 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 6200 1800 6400
Wire Wire Line
	1800 6400 1600 6400
Connection ~ 1600 6400
$Comp
L Connector:TestPoint TP2
U 1 1 601AEA58
P 1350 7150
F 0 "TP2" H 1292 7176 50  0000 R CNN
F 1 "TestPoint" H 1292 7267 50  0001 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1550 7150 50  0001 C CNN
F 3 "~" H 1550 7150 50  0001 C CNN
	1    1350 7150
	-1   0    0    1   
$EndComp
Wire Wire Line
	1350 7150 1350 7100
Wire Wire Line
	1350 7100 1150 7100
$Comp
L Connector:TestPoint TP3
U 1 1 601B18EB
P 1300 2550
F 0 "TP3" H 1358 2668 50  0000 L CNN
F 1 "TestPoint" H 1358 2577 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1500 2550 50  0001 C CNN
F 3 "~" H 1500 2550 50  0001 C CNN
	1    1300 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2550 1300 2850
Text Label 4150 4800 2    50   ~ 0
MCU_D+
Text Label 4150 4900 2    50   ~ 0
MCU_D-
Text Label 1450 6600 0    50   ~ 0
CONN_D+
Text Label 1450 6700 0    50   ~ 0
CONN_D-
$Comp
L Device:R R5
U 1 1 6095FFEC
P 9500 2000
F 0 "R5" H 9570 2046 50  0000 L CNN
F 1 "220" H 9570 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 9430 2000 50  0001 C CNN
F 3 "~" H 9500 2000 50  0001 C CNN
	1    9500 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 60960732
P 9900 2000
F 0 "R4" H 9970 2046 50  0000 L CNN
F 1 "220" H 9970 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 9830 2000 50  0001 C CNN
F 3 "~" H 9900 2000 50  0001 C CNN
	1    9900 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 60964AC2
P 9900 2250
F 0 "#PWR0101" H 9900 2000 50  0001 C CNN
F 1 "GND" H 9905 2077 50  0000 C CNN
F 2 "" H 9900 2250 50  0001 C CNN
F 3 "" H 9900 2250 50  0001 C CNN
	1    9900 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6096525E
P 9500 2250
F 0 "#PWR0102" H 9500 2000 50  0001 C CNN
F 1 "GND" H 9505 2077 50  0000 C CNN
F 2 "" H 9500 2250 50  0001 C CNN
F 3 "" H 9500 2250 50  0001 C CNN
	1    9500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 2250 9500 2150
Wire Wire Line
	9900 2150 9900 2250
$Comp
L Device:LED D1
U 1 1 60969732
P 9900 1650
F 0 "D1" V 9939 1532 50  0000 R CNN
F 1 "LED" V 9848 1532 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9900 1650 50  0001 C CNN
F 3 "https://www.sunledusa.com/products/spec/XZMG53W-1.pdf" H 9900 1650 50  0001 C CNN
	1    9900 1650
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 609748C7
P 9500 1650
F 0 "D2" V 9539 1532 50  0000 R CNN
F 1 "LED" V 9448 1532 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9500 1650 50  0001 C CNN
F 3 "https://www.sunledusa.com/products/spec/XZMG53W-1.pdf" H 9500 1650 50  0001 C CNN
	1    9500 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9500 1800 9500 1850
Wire Wire Line
	9900 1800 9900 1850
$Comp
L power:VBUS #PWR0103
U 1 1 6097E13E
P 9900 1250
F 0 "#PWR0103" H 9900 1100 50  0001 C CNN
F 1 "VBUS" H 9915 1423 50  0000 C CNN
F 2 "" H 9900 1250 50  0001 C CNN
F 3 "" H 9900 1250 50  0001 C CNN
	1    9900 1250
	1    0    0    -1  
$EndComp
Text Label 5700 4900 0    50   ~ 0
LED_PWM
NoConn ~ 5600 5100
NoConn ~ 5600 5700
Text Label 8300 5150 0    50   ~ 0
JUNO_RX_CLOCK_CO
Wire Wire Line
	8300 5050 8250 5050
Text Label 8300 5050 0    50   ~ 0
JUNO_RX_BUSY_SI
$Comp
L power:VBUS #PWR015
U 1 1 609A25D5
P 8300 4150
F 0 "#PWR015" H 8300 4000 50  0001 C CNN
F 1 "VBUS" V 8315 4277 50  0000 L CNN
F 2 "" H 8300 4150 50  0001 C CNN
F 3 "" H 8300 4150 50  0001 C CNN
	1    8300 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 609A5BBC
P 7100 5200
F 0 "#PWR016" H 7100 4950 50  0001 C CNN
F 1 "GND" V 7105 5072 50  0000 R CNN
F 2 "" H 7100 5200 50  0001 C CNN
F 3 "" H 7100 5200 50  0001 C CNN
	1    7100 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 5150 8300 5150
NoConn ~ 9500 6850
$Comp
L power:GND #PWR0104
U 1 1 60A853A8
P 8450 4550
F 0 "#PWR0104" H 8450 4300 50  0001 C CNN
F 1 "GND" V 8455 4422 50  0000 R CNN
F 2 "" H 8450 4550 50  0001 C CNN
F 3 "" H 8450 4550 50  0001 C CNN
	1    8450 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 609C80BB
P 8450 4400
F 0 "C7" H 8565 4446 50  0000 L CNN
F 1 "0.1u" H 8565 4355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 8488 4250 50  0001 C CNN
F 3 "~" H 8450 4400 50  0001 C CNN
	1    8450 4400
	1    0    0    -1  
$EndComp
Text Notes 6900 5950 0    79   ~ 16
Schmitt Trigger Inverter
Wire Notes Line
	10950 5800 9300 5800
Text Notes 9300 5950 0    79   ~ 16
DCB Connector
Wire Notes Line
	8350 2550 8350 950 
Wire Notes Line
	6900 950  6900 2550
Wire Notes Line
	6900 950  8350 950 
Wire Notes Line
	8350 2550 6900 2550
Wire Wire Line
	10150 5000 10150 5500
NoConn ~ 10800 5200
NoConn ~ 10800 5300
NoConn ~ 10300 5300
NoConn ~ 10300 5200
NoConn ~ 10300 5100
Wire Wire Line
	1300 2850 1000 2850
Wire Wire Line
	1000 2850 1000 2800
Connection ~ 1300 2850
Wire Wire Line
	1000 2850 1000 2900
Connection ~ 1000 2850
Wire Notes Line
	2000 5500 850  5500
Wire Notes Line
	850  4100 2000 4100
Text Notes 850  5650 0    79   ~ 16
Crystal
Wire Wire Line
	1000 2500 1000 2300
Wire Notes Line
	1900 2000 850  2000
Wire Notes Line
	850  2000 850  3600
Wire Notes Line
	850  3600 1900 3600
Wire Notes Line
	1900 3600 1900 2000
Text Notes 850  3750 0    79   ~ 16
Reset
Wire Wire Line
	9900 1250 9900 1500
Wire Notes Line
	10200 950  9300 950 
Wire Notes Line
	9300 950  9300 2550
Wire Notes Line
	9300 2550 10200 2550
Wire Notes Line
	10200 2550 10200 950 
Text Notes 9300 2700 0    79   ~ 16
LEDs
Wire Wire Line
	4900 3900 4900 3800
Wire Wire Line
	4900 3800 4800 3800
Connection ~ 4800 3800
Wire Wire Line
	4800 3800 4800 3900
Wire Wire Line
	4900 3800 5000 3800
Wire Wire Line
	5000 3800 5000 3900
Connection ~ 4900 3800
Wire Notes Line
	850  7500 850  5900
Wire Notes Line
	850  5900 2600 5900
Wire Notes Line
	2600 5900 2600 7500
Wire Notes Line
	2600 7500 850  7500
Text Notes 850  7650 0    79   ~ 16
USB
$Comp
L REV_B:TC7WH14FU U2
U 1 1 60A3D7B0
P 7700 5000
F 0 "U2" H 7700 5465 50  0000 C CNN
F 1 "TC7WH14FU" H 7700 5374 50  0000 C CNN
F 2 "REV_B:TC7WH14FU" H 7700 4650 50  0001 C CNN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=20128&prodName=TC7WH14FU" H 7350 5650 50  0001 C CNN
	1    7700 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 4950 8300 4950
Text Label 8300 4950 0    50   ~ 0
JUNO_RX_DATA_DO
Wire Wire Line
	8250 4850 8300 4850
Wire Wire Line
	7100 5200 7100 5150
Wire Wire Line
	7100 5150 7150 5150
Wire Wire Line
	8300 4150 8300 4200
Wire Wire Line
	8450 4250 8450 4200
Wire Wire Line
	8450 4200 8300 4200
Connection ~ 8300 4200
Wire Wire Line
	8300 4200 8300 4850
Wire Wire Line
	6600 6000 6600 4850
Wire Wire Line
	5600 6000 6600 6000
Wire Wire Line
	6700 4950 6700 6100
Wire Wire Line
	5600 6100 6700 6100
Wire Wire Line
	6800 6200 6800 5050
Wire Wire Line
	5600 6200 6800 6200
Wire Wire Line
	7150 5050 6800 5050
Wire Wire Line
	6700 4950 7150 4950
Wire Wire Line
	7150 4850 7100 4850
Wire Notes Line
	6900 3750 6900 5800
Wire Notes Line
	9150 5800 9150 3750
Wire Notes Line
	6900 5800 9150 5800
Wire Notes Line
	6900 3750 9150 3750
Wire Wire Line
	2150 6600 3250 6600
Wire Wire Line
	3350 6700 2150 6700
Wire Wire Line
	1300 2850 2800 2850
Wire Wire Line
	2800 4200 4200 4200
Wire Wire Line
	4800 3250 4800 3300
Wire Wire Line
	4450 3300 4800 3300
Connection ~ 4450 3300
Connection ~ 4800 3300
Wire Wire Line
	4800 3300 4800 3800
Wire Wire Line
	6100 4300 6100 1800
Wire Wire Line
	5600 4300 6100 4300
Wire Wire Line
	6200 1700 6200 4400
Wire Wire Line
	5600 4400 6200 4400
Wire Wire Line
	6300 4500 6300 1600
Wire Wire Line
	5600 4500 6300 4500
Wire Wire Line
	7000 1900 7500 1900
Wire Wire Line
	1700 4600 1700 4800
Wire Wire Line
	1100 4600 1100 4800
Connection ~ 1700 4600
Wire Wire Line
	1700 4600 4200 4600
Wire Wire Line
	1250 4600 1100 4600
Wire Wire Line
	1100 4600 1100 4400
Wire Wire Line
	1100 4400 4200 4400
Connection ~ 1100 4600
Wire Notes Line
	2000 4100 2000 5500
Wire Notes Line
	850  4100 850  5500
Text Label 5700 4300 0    50   ~ 0
ISP_SCK
Text Label 4150 4400 2    50   ~ 0
XTAL1
Text Label 4150 4600 2    50   ~ 0
XTAL2
Wire Wire Line
	3250 4800 4200 4800
Wire Wire Line
	3350 4900 4200 4900
Text Label 4150 5100 2    50   ~ 0
UCAP
Wire Wire Line
	3950 5100 4200 5100
Text Label 7000 1900 0    50   ~ 0
RESET
Wire Wire Line
	3250 4800 3250 6600
Wire Wire Line
	3350 4900 3350 6700
Wire Wire Line
	6100 1800 7500 1800
Wire Wire Line
	6200 1700 7500 1700
Wire Wire Line
	6300 1600 7500 1600
Wire Wire Line
	6400 4900 6400 2850
Wire Wire Line
	6400 2850 8850 2850
Wire Wire Line
	8850 2850 8850 1400
Wire Wire Line
	8850 1400 9500 1400
Wire Wire Line
	5600 4900 6400 4900
Wire Wire Line
	9500 1400 9500 1500
Wire Wire Line
	2800 2850 2800 4200
$Bitmap
Pos 3250 1150
Scale 1.000000
Data
89 50 4E 47 0D 0A 1A 0A 00 00 00 0D 49 48 44 52 00 00 05 89 00 00 00 90 08 06 00 00 00 4D AA E4 
D8 00 00 00 04 73 42 49 54 08 08 08 08 7C 08 64 88 00 00 00 09 70 48 59 73 00 00 2E 18 00 00 2E 
18 01 2A AA 27 20 00 00 11 52 49 44 41 54 78 9C ED DA 7B CC 9E 65 7D 07 F0 6F 4B 01 41 14 58 D1 
29 1E A7 89 DB 92 19 1D 31 F1 84 CE B9 39 74 43 51 52 37 19 71 6E 55 9C 9B A2 09 EA D4 4E 67 EA 
DC B0 03 95 81 73 1E 76 90 19 6D A7 24 88 6E 15 0F 13 F0 04 3A C2 41 05 85 71 92 D2 2A D0 86 72 
90 53 4B D9 1F 0F 89 09 B9 5F 7C DA FE DE F7 BA 9E F7 FE 7C 92 EB BF 27 DF E7 FB BB AF A7 CF FD 
DC 57 DF A4 D6 B6 24 F7 5A DD AE 53 E7 DE BA A6 56 A4 FD B5 19 5A EB E7 73 E8 19 73 66 DA EF C7 
D0 3A B2 70 C6 75 1D CC 33 B4 56 16 CE 38 06 6B D2 7E CF 86 D6 EA C2 19 57 75 30 CF D0 3A A9 70 
C6 31 38 3A ED F7 6C 68 9D 5E 38 E3 E1 1D CC 33 B4 CE 2A 9C F1 D0 0E E6 B1 76 7F 6D B8 FF C6 CE 
98 93 D2 FE 1A 0E AD 55 85 33 AE EE 60 9E A1 B5 A6 70 C6 31 58 99 F6 7B 36 B4 D6 CD E7 D0 BB E1 
C5 69 7F 6D AC 07 5E 47 CC B9 7B 6D AD 4D FB 6B 63 ED FE 7A FB FD 37 76 BE 2D 5D E8 37 04 00 00 
00 00 A0 1F 0E 89 01 00 00 00 00 46 CC 21 31 00 00 00 00 C0 88 39 24 06 00 00 00 00 18 31 87 C4 
00 00 00 00 00 23 E6 90 18 00 00 00 00 60 C4 1C 12 03 00 00 00 00 8C 98 43 62 00 00 00 00 80 11 
73 48 0C 00 00 00 00 30 62 0E 89 01 00 00 00 00 46 CC 21 31 00 00 00 00 C0 88 2D 2B CE FB CC 3C 
64 52 E7 3B AD 0B CC 61 43 26 9F 9D DE 5C D8 BA 40 47 CE 49 72 73 EB 12 03 AE 2B CC FA 76 92 25 
85 79 55 AE 6A 5D 60 C6 5C 9C 3E BF 4F 7E 50 98 75 69 FA 9C D1 77 E6 CE F9 71 FA DC C7 EF 16 66 
6D 4A 9F 33 5E 52 98 75 63 FA 9C 91 9D B3 B9 75 81 DD 74 61 FA FC 1C 5E 5A 98 F5 83 F4 39 E3 C5 
AD 0B CC 98 AB D2 E7 3E 7E BB 75 81 39 6C 4C 9F D7 8B 9F DB D8 BA C0 1C CE 8D 3F 0A 5D 0C 7E D8 
BA 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 33 68 49 92 CB 5A 97 98 C3 53 92 DC D9 
BA 44 07 56 24 F9 BB D6 25 06 FC 57 92 37 17 65 1D 96 E4 E4 A2 AC 4A E7 24 79 6D EB 12 9D F8 78 
92 E7 B6 2E 31 E0 0D 49 BE 52 94 F5 81 24 7F 50 94 B5 D0 AE 4F 9F FB 53 69 DF 24 17 B6 2E B1 1B 
3E 94 E4 94 A2 AC 3F 4F 72 5C 51 56 0B 4F 4F B2 B5 75 89 79 F6 D5 24 8F 69 5D 62 17 7D 25 93 EF 
D6 0A CF 4B F2 D1 A2 AC 4A E7 25 79 55 51 D6 21 49 D6 16 65 55 BA 24 C9 91 AD 4B 74 E2 F8 F4 79 
2D 56 27 F9 74 51 D6 AA D4 7D A6 2B 7D 20 75 DF 01 C7 A6 EE BB A9 85 DF 4C 72 7B EB 12 F3 EC 9C 
24 8F 68 5D 62 17 FD 77 EA 7E 5B BD 20 93 DF 7D BD F9 7A 92 63 5A 97 E8 C4 C7 92 FC 56 EB 12 03 
DE 98 E4 4B 45 59 EF 4F 72 78 51 56 A5 BF 4E 72 5A EB 12 1D 78 50 92 8B 5B 97 18 B2 2C C9 93 5A 
97 98 C3 D2 D6 05 3A 71 40 FA DC A3 83 0B B3 1E 92 3E 67 BC B2 75 81 8E 3C 26 7D EE D1 43 0A B3 
0E 4E 9F 33 4E 63 BF D6 05 16 C0 D2 CC EE FE 24 C9 41 85 59 CB 33 DB D7 62 8F D6 05 16 C0 13 92 
FC 4A EB 12 BB E8 D2 C2 AC FD D2 E7 67 75 53 61 D6 BE E9 73 C6 9F B5 2E D0 91 47 A6 CF 3D 3A B0 
30 EB E1 E9 73 C6 E5 85 59 07 A5 CF 19 A7 35 86 67 DB 27 26 79 54 EB 12 BB A8 F2 0F 11 7A 7D B6 
BD BA 75 81 8E 3C 3A 7D EE D1 18 9E 6D 0F 68 5D A0 13 DD 3E DB 8E E1 66 05 00 00 00 00 C0 1C 1C 
12 03 00 00 00 00 8C 98 43 62 00 00 00 00 80 11 73 48 0C 00 00 00 00 30 62 0E 89 01 00 00 00 00 
46 CC 21 31 00 00 00 00 C0 88 39 24 06 00 00 00 00 18 31 87 C4 00 00 00 00 00 23 E6 90 18 00 00 
00 00 60 C4 1C 12 03 00 00 00 00 8C D8 B2 24 17 B7 2E 31 87 1D AD 0B 74 62 4B FA DC A3 6B 0A B3 
B6 A6 CF 19 AF 6C 5D A0 23 57 A6 CF 3D DA 5A 98 75 4D FA 9C 71 1A 37 B4 2E B0 00 EE C9 EC EE 4F 
92 FC A4 30 EB FA CC F6 B5 D8 DE BA C0 02 B8 34 C9 2D AD 4B EC A2 AB 0B B3 6E 4E 9F 9F D5 2B 0A 
B3 6E 4B 9F 33 5E D6 BA 40 47 AE 4D 9F 7B 74 63 61 D6 C6 F4 39 E3 F5 85 59 3F 49 9F 33 4E EB 9E 
D6 05 16 C0 25 49 36 B7 2E B1 8B AE 29 CC F2 6C DB 3F CF B6 ED 6C 69 5D A0 13 3B D2 E7 FE 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 B0 18 2C 49 72 46 EB 12 73 78 79 92 BB 5B 97 E8 C0 
0B 92 BC A1 75 89 01 5F 4F F2 FE A2 AC 67 25 79 5B 51 56 A5 F3 93 FC 6D EB 12 9D 78 77 92 43 5A 
97 18 70 7C 92 F3 8A B2 DE 92 E4 39 45 59 BD FA 70 92 2F B5 2E 31 60 65 92 23 5A 97 98 67 EB 92 
AC 2D CA 5A 91 E4 95 45 59 BD 5A 9F E4 A3 AD 4B 0C 78 7E 92 37 B5 2E 31 CF CE 4D F2 BE A2 AC A7 
25 79 57 51 56 A5 EF 27 79 67 51 D6 AF A7 EE 7A 55 BA 32 C9 71 AD 4B 74 E2 8D 49 7E A7 75 89 01 
1F 49 F2 C5 A2 AC 57 27 79 49 51 56 A5 4F 26 39 AD 28 EB A8 24 AF 28 CA EA D5 19 49 FE AD 75 89 
01 87 25 F9 CB D6 25 E6 D9 37 92 9C 58 94 F5 8C 24 EF 28 CA AA 74 41 92 D5 AD 4B 74 E2 5D 99 FC 
46 E9 CD 9A 24 DF 2E CA 7A 73 92 E7 16 65 55 FA 50 92 AF B4 2E D1 81 BD 92 7C B6 75 89 21 CB D2 
E7 0F 8A 64 D2 CD 21 71 F2 B8 F4 B9 47 B7 17 66 1D 9C 3E 67 DC B3 75 81 8E 3C 33 93 1F 88 BD 39 
B5 30 EB 69 E9 F3 73 58 69 7D EB 02 73 78 72 16 FF B5 BF A8 30 EB 49 59 FC D7 6B 63 EB 02 73 78 
6C 16 FF B5 DF 51 98 F5 88 F4 79 BD 1E 5A 98 B5 3C 7D CE 78 61 EB 02 1D 39 24 7D EE D1 97 0B B3 
7A BD 8F 7E A7 30 EB D7 D2 E7 8C 95 AE 6A 5D 60 0E 8F CF E2 BF F6 77 14 66 F5 FA 6C BB 77 EB 02 
1D 79 66 92 17 B5 2E 31 E0 93 85 59 BD 3E DB 7E A1 75 81 4E 74 7B 16 BB B4 75 01 00 00 00 00 00 
DA 71 48 0C 00 00 00 00 30 62 0E 89 01 00 00 00 00 46 CC 21 31 00 00 00 00 C0 88 39 24 06 00 00 
00 00 18 31 87 C4 00 00 00 00 00 23 E6 90 18 00 00 00 00 60 C4 1C 12 03 00 00 00 00 8C 98 43 62 
00 00 00 00 80 11 73 48 0C 00 00 00 00 30 62 CB 92 DC D5 BA C4 1C EE 6D 5D A0 13 DB D3 E7 1E DD 
5D 98 75 4F 16 FF 8C B3 EE EE F4 B9 47 F7 14 66 F5 3A 63 A5 ED AD 0B CC 61 5B 16 FF B5 DF 56 98 
D5 EB 7D A1 52 E5 F5 AA D4 EB FD AA 92 FB FB CE D9 91 3E 67 EC B1 53 2B BD DE 63 2A EF C9 66 5C 
1C 7A BD F7 8D E1 77 87 7B DF B8 F4 FA DC 37 86 67 DB 5E 9F 47 17 DA BD E9 73 7F 00 00 00 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
00 58 7C 96 24 79 5C EB 12 73 B8 36 C9 BD AD 4B 74 60 BF 24 CB 5B 97 18 F0 B3 24 9B 8B B2 F6 4D 
F2 B0 A2 2C C6 E5 86 24 77 14 65 1D 94 E4 C1 45 59 95 36 67 F2 EF ED 81 EC 91 E4 D1 0B D0 A5 A5 
1D 49 36 4C F1 BA 03 92 EC 3F CF 5D 76 C5 D6 24 37 17 65 3D 34 C9 81 45 59 95 6E 49 72 D3 14 AF 
7B 74 26 9F D9 C5 6C 63 92 ED BF E0 35 0F CE E4 7B A7 37 B7 27 B9 B1 28 6B 9F 24 0F 2F CA AA 74 
67 92 EB 8B B2 F6 4E F2 88 A2 AC 4A 77 25 F9 69 EB 12 9D 58 9E C9 EF E9 DE 6C 49 72 5B 51 D6 81 
99 DC 1B 7A 73 53 26 F7 86 0A FB 67 72 8F EF CD CD 99 DC E3 7F 91 C7 24 59 3A CF 5D 5A BB 2E C9 
3D BF E0 35 BD DE FB 2A 9F 6D 7B BD F7 DD 91 C9 B3 13 93 FD D9 A7 75 89 01 37 66 F2 3B AC 42 AF 
CF B6 95 F7 BE 59 B6 24 C9 63 5B 97 98 CB BD 9D AE 7D E7 73 E8 19 F2 9A B4 DF 8B A1 B5 B6 70 C6 
15 1D CC 63 CD E6 3A 32 75 D6 75 30 CF D0 5A 39 45 F7 47 75 D0 73 BE D7 B4 3F 26 D6 74 D0 75 68 
AD 9E B2 FF 34 56 75 30 CF D0 3A 69 CA FE 5B 3A E8 3A DF EB 09 53 5C 87 A3 3B E8 39 B4 4E 9F A2 
FB B4 0E EF 60 9E A1 75 56 E1 8C 87 76 30 CF D0 BA A0 70 C6 59 F7 89 B4 DF 8F A1 F5 FA C2 19 4F 
EA 60 9E A1 B5 AA 70 C6 D5 1D CC 33 B4 D6 4C D9 FF B6 0E BA CE F7 9A E6 0F 16 56 76 D0 73 68 AD 
9B A2 FB B4 8E EC 60 9E A1 75 66 E1 8C B3 6E 7D DA EF C7 D0 5A 51 38 E3 DA 0E E6 19 5A AF 29 9C 
71 96 ED 9B F6 7B 31 B8 16 FB FF 66 02 00 00 00 00 F0 00 1C 12 03 00 00 00 00 8C 98 43 62 00 00 
00 00 80 11 73 48 0C 00 00 00 00 30 62 0E 89 01 00 00 00 00 46 CC 21 31 00 00 00 00 C0 88 39 24 
06 00 00 00 00 18 31 87 C4 00 00 00 00 00 23 E6 90 18 00 00 00 00 60 C4 1C 12 03 00 00 00 00 8C 
98 43 62 00 00 00 00 80 11 5B 96 E4 84 D6 25 E6 B0 AD 75 81 4E 5C 9C 3E F7 E8 A2 C2 AC CB D2 E7 
8C F4 EF F2 C2 AC CF 27 B9 B6 30 AF CA F7 A6 78 CD AD 59 FC FF 86 EE 9A F2 75 67 27 59 32 8F 3D 
76 D5 37 0B B3 CE 4D 9F FB FD 8D 29 5F 77 72 92 07 CF 67 91 0E 6C 9D E2 35 97 A6 CF 7D FC 41 61 
D6 15 E9 73 C6 2B 0A B3 AE 4B 9F 33 6E 6C 5D A0 23 EB 93 DC D0 BA C4 80 0B 0A B3 FE 27 C9 DD 85 
79 55 CE 2D CC FA 66 FA FC B7 76 F6 94 AF FB 60 92 BD E7 B1 47 0F 6E 9D E2 35 DF 4B 9F FB 58 F9 
6C 7B 79 FA 9C F1 B2 D6 05 3A 72 5A 6A 7F EF 54 A9 DC A3 2F 24 D9 50 98 57 E5 E2 D6 05 3A B1 2D 
7D 7E 4F 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 B0 18 2C 49 72 67 EB 12 73 38 30 C9 
1D AD 4B 74 E0 4F 93 7C A4 75 89 01 FF 99 E4 55 45 59 2F 4B B2 B6 28 AB D2 99 49 5E DA BA 44 27 
3E 9F E4 F7 5A 97 18 F0 47 49 CE 28 CA FA 8F 24 7F 58 94 55 E9 B5 99 74 AB F0 A1 24 AF 29 CA AA 
F4 D6 24 A7 14 65 BD 37 C9 5B 8A B2 2A BD F7 BE 55 E1 AF 92 BC A7 28 AB D2 29 99 EC 65 85 D7 26 
39 B9 28 AB D2 27 93 1C 53 94 F5 8A 24 9F 28 CA AA 74 46 26 DF AD 15 5E 94 E4 F4 A2 AC 4A E7 24 
39 AC 28 EB 59 49 BE 56 94 55 E9 C2 24 CF 6C 5D A2 13 1F 4F F2 CA D6 25 06 BC 29 C9 47 8B B2 4E 
48 72 6C 51 56 A5 BF 49 F2 0F 45 59 EF BC 6F F5 E6 C4 D4 F5 3A 36 93 BD EC CD BF 24 79 43 51 D6 
9F 24 F9 58 51 56 A5 CF 64 D2 AD C2 11 99 3C 2B F7 E6 CB 49 5E D2 BA 44 27 3E 97 E4 85 AD 4B 0C 
38 2A 75 BF 9B 4E 4D DD EF B9 4A AF 4B 9F BF 7F 17 DA 3E 49 6E 6A 5D 62 C8 B2 24 7B B7 2E 31 87 
25 AD 0B 74 A2 D7 3D DA AB 30 6B 8F 2C FE 19 67 DD 5E E9 73 8F F6 28 CC EA 75 C6 65 85 59 7B C6 
8C AD EC 59 98 D5 EB 7D C1 8C 3B 67 0C F7 BE 31 CC B8 34 7D CE D8 63 A7 56 7A BD 2F 8C E1 DE 37 
86 19 DD FB 76 4E AF 33 BA F7 8D 4B AF CF 7D 9E 6D C7 63 49 FA DC 9F 2C 6D 5D 00 00 00 00 00 80 
76 1C 12 03 00 00 00 00 8C 98 43 62 00 00 00 00 80 11 73 48 0C 00 00 00 00 30 62 0E 89 01 00 00 
00 00 46 CC 21 31 00 00 00 00 C0 88 39 24 06 00 00 00 00 18 31 87 C4 00 00 00 00 00 23 E6 90 18 
00 00 00 00 60 C4 1C 12 03 00 00 00 00 8C D8 B2 24 9F 6F 5D 62 0E DB 5B 17 E8 C4 8F D3 E7 1E 9D 
5F 98 B5 29 8B 7F C6 59 77 6E 92 BB 5A 97 18 B0 A9 30 EB FC 24 FB 14 E6 55 F9 71 61 D6 45 E9 F3 
DF DA 95 85 59 97 A4 CF 19 7F 54 98 75 79 FA 9C F1 FB 85 59 57 A7 CF 19 2F 28 CC DA 90 3E 67 FC 
4E 61 D6 4F D3 E7 8C 95 9F D5 2D E9 73 C6 CA EF D5 59 77 41 92 03 5B 97 18 70 55 61 D6 F7 D3 E7 
E7 F0 F2 C2 AC 1F A5 CF 19 2F 29 CC BA 32 7D CE 78 51 61 96 67 DB 76 2A 7F C3 CC BA 73 93 6C 6B 
5D 62 40 F5 B3 ED BE 85 79 55 2A 9F 6D 67 D9 F6 F4 F9 3D 01 00 00 00 00 00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
00 00 00 00 C0 62 B0 24 C9 45 AD 4B CC E1 19 49 EE 6C 5D A2 03 2F 4B F2 EE D6 25 06 7C 31 C9 3B 
8A B2 7E 37 C9 89 45 59 95 BE 91 E4 D8 D6 25 3A F1 4F 49 9E DD BA C4 80 E3 92 7C AD 28 EB 7D 49 
5E 58 94 55 E9 DD 49 CE 58 C0 F7 3B 30 C9 59 45 59 B7 24 79 6E 51 D6 B4 DE 94 E4 CF 16 F8 3D A7 
F1 91 FB 56 85 57 A7 CF EF A6 4F 25 39 61 81 DF F3 AB 49 0E 2A CA 3A 2C C9 F5 45 59 D3 78 51 92 
E3 17 F0 FD A6 F5 B5 4C BE 5B 2B 3C 27 C9 29 45 59 95 FE 37 C9 31 45 59 4F 4D F2 89 A2 AC 4A 3F 
4C 72 54 EB 12 9D 78 4F 92 97 B4 2E 31 E0 EF 93 7C A6 28 EB AD 49 8E 2E CA AA 74 4A 92 7F 2D CA 
7A DD 7D AB 37 FF 9E E4 1F 17 F8 3D BF 9E E4 A1 45 59 BF 9D E4 A6 A2 AC 69 1C 91 64 F5 02 BE DF 
B4 CE 4C F2 F6 A2 AC E7 27 F9 40 51 56 A5 6F 25 79 7D EB 12 9D 38 25 93 DF 28 BD 79 4B 26 BF 6D 
2B 1C 9F C9 6F CD DE AC 4E 72 7A EB 12 1D 78 50 92 F3 5A 97 18 B2 2C C9 53 5A 97 98 C3 D2 D6 05 
3A B1 3C 7D EE D1 0F 0B B3 0E 48 9F 33 6E 6A 5D A0 23 4F 4C 9F 7B 74 40 61 D6 E3 D3 E7 8C CB 17 
F8 FD F6 4C DD 75 D8 5A 94 B3 33 0E 4E 9F FB F8 C8 C2 AC 5F 4E 9F 33 9E DD E0 3D 7F 23 93 EB 51 
61 AF A2 9C 69 FD 52 FA DC C7 AB 0B B3 F6 4F 9F 33 56 1E 88 EC 97 3E 67 DC D1 BA 40 47 1E 9B 3E 
F7 E8 61 85 59 8F 4A 9F 33 56 7D 3F 27 93 FB 68 8F 33 1E DC E0 3D 9F 9C BA DF C0 7B 16 E5 4C AB 
D7 67 DB 1F 15 66 F5 FA 6C FB D3 D6 05 3A E2 D9 B6 9D 85 7E B6 ED D5 D2 F4 B9 3F 0E 62 01 00 00 
00 00 C6 CC 21 31 00 00 00 00 C0 88 39 24 06 00 00 00 00 18 31 87 C4 00 00 00 00 00 23 E6 90 18 
00 00 00 00 60 C4 1C 12 03 00 00 00 00 8C 98 43 62 00 00 00 00 80 11 73 48 0C 00 00 00 00 30 62 
0E 89 01 00 00 00 00 46 CC 21 31 00 00 00 00 C0 88 2D 4B 72 79 EB 12 73 D8 D1 BA 40 27 B6 A6 CF 
3D DA 54 98 75 6B FA 9C F1 BA D6 05 3A B2 21 7D EE D1 AD 85 59 9B D2 E7 8C 37 2F F0 FB 6D 4F DD 
75 58 E8 EE 49 72 63 FA DC C7 CD 85 59 5B D2 E7 8C 37 34 78 CF 2B 52 F7 39 DB 56 94 33 AD 5B D2 
E7 3E 6E 2C CC BA 2D 7D CE 78 6D 61 D6 ED E9 73 C6 6B 5A 17 E8 C8 4F D2 E7 1E DD 54 98 75 43 FA 
9C 71 4B 61 D6 E6 F4 39 E3 8D 0D DE F3 FF 92 EC 5F 94 B5 BD 28 67 5A 37 A7 CF 7D 1C C3 B3 ED 86 
D6 05 3A 72 5D FA DC A3 31 3C DB 6E 6D 5D A0 13 3B D2 E7 FE 00 00 00 00 00 00 00 00 00 00 00 00 
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
00 00 00 00 00 B0 18 2C 29 CE FB 54 92 65 C5 99 D4 39 27 C9 87 5B 97 18 F0 F4 24 C7 B5 2E 31 E0 
C2 24 EF 6B 5D A2 13 EF 48 F2 D4 D6 25 06 BC 3F C9 77 8B B2 DE 98 E4 D9 45 59 95 FE 39 C9 D9 AD 
4B CC 90 3F 4E 72 44 EB 12 03 4E 4B F2 D9 A2 AC 97 26 39 AA 28 AB D2 FA 24 A7 B6 2E 31 43 0E 4D 
72 6C EB 12 03 BE 9B C9 77 6B 85 43 92 BC AD 28 AB D2 25 49 DE 53 94 F5 AB 85 59 B4 B3 39 C9 EB 
5B 97 D8 0D AF 4A F2 FB AD 4B 0C 58 9B E4 73 45 59 2F 4F B2 A2 28 AB D2 19 49 3E DD BA C4 0C 79 
5E 92 BF 68 5D 62 C0 B7 92 9C DC BA C4 80 5E EF A3 FC DC 09 49 CE 6F 5D 62 40 AF CF B6 EC 9C 4F 
67 72 9F 99 59 DB 92 DC 6B 75 BB 7A 7D 78 5F 91 F6 D7 66 68 AD 9F CF A1 67 CC 99 69 BF 1F 43 EB 
C8 C2 19 D7 75 30 CF D0 5A 59 38 E3 18 AC 49 FB 3D 1B 5A AB 0B 67 5C D5 C1 3C 43 EB A4 C2 19 C7 
E0 E8 B4 DF B3 A1 75 7A E1 8C 87 77 30 CF D0 3A AB 70 C6 43 3B 98 C7 DA FD B5 E1 FE 1B 3B 63 4E 
4A FB 6B 38 B4 56 15 CE B8 BA 83 79 86 D6 9A C2 19 C7 60 65 DA EF D9 D0 5A 37 9F 43 EF 86 17 A7 
FD B5 B1 1E 78 F5 F8 C7 29 C9 E4 3F E9 5A 5F 1B 6B F7 D7 DB EF BF B1 F3 6D E9 42 BF 21 00 00 00 
00 00 FD 70 48 0C 00 00 00 00 30 62 0E 89 01 00 00 00 00 46 CC 21 31 00 00 00 00 C0 88 39 24 06 
00 00 00 00 18 31 87 C4 00 00 00 00 00 23 E6 90 18 00 00 00 00 60 C4 1C 12 03 00 00 00 00 8C 98 
43 62 00 00 00 00 80 11 73 48 0C 00 00 00 00 30 62 0E 89 01 00 00 00 00 46 EC FF 01 BB F7 0A E1 
AF 36 09 C6 00 00 00 00 49 45 4E 44 AE 42 60 82 
EndData
$EndBitmap
$Comp
L Device:CP C3
U 1 1 60DC3CF0
P 3950 5400
F 0 "C3" H 4068 5446 50  0000 L CNN
F 1 "1u" H 4068 5355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3988 5250 50  0001 C CNN
F 3 "~" H 3950 5400 50  0001 C CNN
	1    3950 5400
	1    0    0    -1  
$EndComp
Text Notes 5750 5850 0    50   ~ 0
PD3 -> TXD1\nPD4 -> INT5\nPD5 -> XCK1
Text Notes 5750 5600 0    50   ~ 10
Port D Pin Functions
Text Notes 5150 3850 0    50   ~ 0
PB1 -> SCK\nPB2 -> PDI\nPB3 -> PDO\nPB7 -> OC0A
Text Notes 5150 3500 0    50   ~ 10
Port B Pin Functions
Wire Notes Line
	9300 3750 10950 3750
Wire Notes Line
	10950 3750 10950 5800
Wire Notes Line
	9300 3750 9300 5800
Wire Wire Line
	10100 4900 10300 4900
Wire Wire Line
	10100 4800 10300 4800
Wire Wire Line
	10100 4700 10200 4700
$Comp
L Device:R R6
U 1 1 60AD383B
P 10200 4400
F 0 "R6" H 10000 4450 50  0000 L CNN
F 1 "100k" H 9900 4350 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 10130 4400 50  0001 C CNN
F 3 "~" H 10200 4400 50  0001 C CNN
	1    10200 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 4550 10200 4700
Connection ~ 10200 4700
Wire Wire Line
	10200 4700 10300 4700
$Comp
L power:VBUS #PWR010
U 1 1 60ADD168
P 10200 4150
F 0 "#PWR010" H 10200 4000 50  0001 C CNN
F 1 "VBUS" V 10215 4277 50  0000 L CNN
F 2 "" H 10200 4150 50  0001 C CNN
F 3 "" H 10200 4150 50  0001 C CNN
	1    10200 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 4150 10200 4250
Text Label 5750 6000 0    50   ~ 0
USART1_TX
Text Label 5750 6200 0    50   ~ 0
USART1_CLK
Text Label 5750 6100 0    50   ~ 0
PD4
Text Notes 6900 2700 0    79   ~ 16
ISP Connector
$Comp
L Connector:TestPoint TP4
U 1 1 60B90A2F
P 7100 4200
F 0 "TP4" H 7158 4318 50  0000 L CNN
F 1 "TestPoint" H 7158 4227 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 7300 4200 50  0001 C CNN
F 3 "~" H 7300 4200 50  0001 C CNN
	1    7100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4200 7100 4850
Connection ~ 7100 4850
Wire Wire Line
	7100 4850 6600 4850
$EndSCHEMATC
